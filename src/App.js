import React from 'react';
import './App.css';

// import Table from './table/Table';
// import TableHead from './table/TableHead';
// import TableBody from './table/TableBody';
// import TableData from './table/TableData';

// import { HtmlInput, Input, HtmlButton, Button, ToDoList, } from './tasks_task.2';
import { Form, RegisterForm, MultiSelectWithConfirmation } from './tasks_task.3.forms';

// const onButtonClick = event => {
//   console.log('kliknięto przycisk');
// }
// const label = 'Wciśnij mnie';

const App = () => { 
  return (
    <>
    {/* <Table>
      <TableHead data = {TableData} />
      <TableBody data = {TableData} />
    </Table> */}
    <div className='form'>
      <Form />
      <RegisterForm />
      <MultiSelectWithConfirmation />
    </div>
    {/* <div className='input'>
      <HtmlInput />
      <Input />
    </div>
    <div className='button'>
      <HtmlButton />
      <Button label={label} onClick={onButtonClick}/>
    </div>
    <div className='list'>
      <ToDoList />
    </div> */}
    </>
  )
}

export default App;
