import React from 'react';

const TableHead = props => {
  const keys = Object.keys(props.data[0])
  const headers = keys.map((item, key) => (<th key={key}>{item}</th>))

  return (
    <thead>
      <tr>
        {headers}
      </tr>
    </thead>
  )
  
};

export default TableHead;