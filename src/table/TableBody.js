import React from 'react';

const TableBody = props => {
  const rows = props.data.map((row, index) => {
    return (
      <tr key={index}>
        <td>{row.name}</td>
        <td>{row.amount}</td>
      </tr>
    )
  })

  return <tbody>{rows}</tbody>
}

export default TableBody;