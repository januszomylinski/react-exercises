import React from 'react';

// "stwórz formularz do podawania płci i wieku"

// Część 1
// input na wiek ma być typu range od 16 do 65 lat
// input na płeć ma być typu select i zawierać M lub K


class Age extends React.Component {
    state = {
        value: 16
    }
    
    handleChange = event => {
        this.setState({value: event.target.value});
        this.props.changeAge(event.target.value);
    }
    
    render(){
        const { value } = this.state;
        return(
            <>
            <div>Wybierz wiek: {value}</div>
            <input 
                type='range' 
                min={16} 
                max={65} 
                value={value}
                onChange={this.handleChange} />
            </>
        )
    }   
}

class Sex extends React.Component {
    state = {
        value: ''
    }

    handleChange = event => {
        this.setState({value: event.target.value});
        this.props.changeSex(event.target.value);
    }

    render() {
        const { value } = this.state;
        return(
            <select value={value} onChange={this.handleChange} action={this.value}>
                <option value=''>Wybierz płeć:</option>
                <option value='K'>Kobieta</option>
                <option value='M'>Mężczyzna</option>
                <option value='D'>Inna</option>
            </select>
        )
    }
}

// Część 2
// komponent Form ma zawierać 1 metodę do zmiany stanu w do obu komponentów
// komponent Form ma zawierać metodę onSubmit która loguje w konsoli wartość stanu na wysyłce formularza

class Form extends React.Component {
    state = {
        age: '',
        sex: ''
    }

    onSubmit = event => {
        console.log(this.state.age);
        console.log(this.state.sex);
        event.preventDefault();
    }
    
    changeAge = newAge => {
        this.setState({age: newAge});
    }

    changeSex = newSex => {
        this.setState({sex: newSex});
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <h1>Formularz1:</h1>
                <Age changeAge={this.changeAge}/><br />
                <Sex changeSex={this.changeSex}/><br />
                <input type="submit" value="Wyślij" />
            </form>
        )
    }
}

// "stwórz formularz do rejestracji usera"

// Część 1
// formularz ma posiadać 4 inputy o nazwach odpowiednio:
// - email, password, confirm-password oraz agreement
// o typach odpowiednio:
// - email, password, password, checkbox
// komponent RegisterForm ma zawierać 1 metodę do zmiany stanu w do obu komponentów

// Część 2 
// komponent RegisterForm ma zawierać metodę onSubmit, która dokonuje walidacji:
// - email - email
// - password/confirm-password - jest dłuższe niż 8 znaków 
// - inputy na hasła mają taką samą wartość
// - agreement musi mieć wartość true

// Część 3
// jeśli walidacja nie przejdzie, trzeba wyświetlić komunikat błędu "ValidationError"
// jeśli walidacja się powiedzie, trzeba wyświeltić komunikat "User created"

class RegisterForm extends React.Component{
    state = {
        email: '',
        password: '',
        confirmPassword: '',
        agreement: false,
    }

    
    onSubmit = event => {
        console.log(event.target)
        event.preventDefault();
        let { email, password, confirmPassword, agreement } = this.state;
        const isEmailValid = ( email !== '' ) && ( email.includes('@') );
        const isPasswordValid = password.length > 8;
        const isConfirmPasswordValid = ( confirmPassword.length > 8 ) && ( confirmPassword === password );
        const isAgreementValid = agreement === true;
        const conditions = [isEmailValid, isPasswordValid, isConfirmPasswordValid, isAgreementValid]
        const areConditionsFalse = conditions.includes(false);
        if ( areConditionsFalse === true) { 
            alert("ValidationError");
        } else {
            alert("User created");
        }
    }
    
    handleChange = event => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    render() {
        return(
            <form onSubmit={this.onSubmit}>
                <h1>Formularz2:</h1>
                <input type="email" name="email" placeholder="Wpisz email" value={this.state.email} onChange={this.handleChange} /><br />
                <input type="password" name="password" placeholder="Wpisz hasło" value={this.state.password} onChange={this.handleChange} /><br />
                <input type="password" name="confirmPassword" placeholder="Potwierdź hasło" value={this.state.confirmPassword} onChange={this.handleChange} /><br />
                <input type="checkbox" name="agreement" checked={this.state.agreement} onChange={this.handleChange} />
                <span>Zgoda marketingowa</span><br />
                <input type="submit" value="Wyślij" />
            </form>
        )
    }
}

// "stwórz bardziej zaawansowany komponent typu input"

// Część 0:
// state: {
// options: [array z elementami o wzorcu {label:'Nazwa do selecta', value:'Wartość do selecta'}]
// selected: [array z elementami o wzorcu {label:'Nazwa do selecta', value:'Wartość do selecta'}]
// }

// Część 1
// komponent ma zawierać:
// - select z 10 dowolnymi opcjami przetrzymywany w stanie pod kluczem "options"
// - tyle inputów typu checkbox ile zostało dokonane wyborów z selecta
// - przycisk do wysłania wyborów ze stanu o kluczu "selected"


// Część 2
// jak to ma działać:
// 1. user wybiera element z selecta
// 1a. wybrany element wrzucany jest do stanu { selected:[element0] }
// 2. po wybraniu, nazwa element0 pokazuje się poniżej z checkboxem(checked=true)
// 3. jeśli user wybierze kolejny element to też jest on wrzucany do stanu { selected:[element0, element1] }
// 3a. pokazuje się poniżej element1 z checkboxem(checked=true)
// 4. jeśli user kliknie w checkbox na dowolny element listy to ten element znika z listy


// jeśli user wcisnie przycisk w inputcie, do konsoli ma być logowany stan "selected" a potem stan "selected" ma być czyszczony
// user nie może wybrać 2 razy tego samego elementu - lista checkboxów ma nie mieć duplikatów

class MultiSelectWithConfirmation extends React.Component{
    state = {
        options: [
            { label: 'Franek', value: 76 },
            { label: 'Zbychu', value: 225 },
            { label: 'Rudy', value: 42 },
            { label: 'Maniek', value: 900 },
            { label: 'Lodi', value: 500 },
            { label: 'Młody Lodi', value: 19 },
            { label: 'Kinia', value: 100 },
            { label: 'Martuś', value: 120 },
            { label: 'Jędza', value: 80 },
            { label: 'Dzidzia', value: 38 },
        ],
        selected: [],
    }

    handleSelectChange = e => {
        let { selected } = this.state;
        selected = [...selected, {value: e.target.value}]
        selected = selected.filter((obj, idx, arr) => (
            arr.findIndex((o) => o.value === obj.value) === idx
        )); 
        this.setState({ selected: selected })
    }
    
    handleSubmit = e => {
        e.preventDefault();
        console.log(this.state);
        this.setState({selected: []});
    }

    handleCheckboxClick = e => {
        let { selected } = this.state;
        const value = e.target.value;
        selected = selected.filter(i => i.value !== value);
        this.setState({selected: selected});
    }


    render() {
        const { options, selected } = this.state

        return(
            <form onSubmit={this.handleSubmit}>
                <h1>Formularz3</h1>
                <select onChange={this.handleSelectChange}>
                    <option value=''>Wybierz:</option>
                    {options.map((el, i) => <option value={el.value} label={el.label} key={i}>{el.label}</option>)}
                </select>
                <h2>Wybrane:</h2>
                <ul>
                    {selected.map((el, i) => 
                    <li key={i}>
                    <input type='checkbox' name={el.label} value={el.value} checked='true' onClick={this.handleCheckboxClick}></input>{el.value}
                    </li>
                    )} 
                </ul><br /><br />
                <input type='submit' value='Wyślij wybrane' />
            </form>
        )
    }
}

export {
    Age,
    Sex,
    Form,
    RegisterForm,
    MultiSelectWithConfirmation,
};