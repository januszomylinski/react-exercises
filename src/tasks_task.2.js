import React from 'react';
import axios from 'axios';
// 1. z podanego htmla wytworzyć aktywnie działające komponenty stanowe

class HtmlInput extends React.Component {
    render() {
        return(
            <div>
                <input placeholder='HtmlInput' />
            </div>
        )
    }
}

class Input extends React.Component {
    // w tym komponencie wartosc inputa ma być przechowywana w stanie pod kluczem "inputValue"
    // ten komponent ma mieć metodę onInputChange która loguje zmiany wartości do konsoli
    
    constructor() {
        super(); // tego nie do końca rozumiem, sprawdzić po co to
        this.state = {inputValue: ''};
        this.onInputChange = this.onInputChange.bind(this); // tego nie do końca rozumiem, sprawdzić po co to
    }

    onInputChange(event) {
        this.setState({inputValue: event.target.value});
        console.log(event.target.value);
    }
    
    render() { 
        return (
            <div>
                <input placeholder='Input' value={this.state.value} 
                onChange={this.onInputChange} />
            </div>
        )
    }
}



// // 2. z podanego htmla wytworzyć aktywnie działające komponenty funkcyjne

const HtmlButton = () => {
    return (
    <div>
        <button>Wciśnij mnie</button>
    </div>
    )
}

// // ten komponent ma otrzymywać propsy "label" i "onButtonClick"
// // "label" ma być wyrenderowany jako label przycisku
// // "onButtonClick" ma być przyczepiony do eventu onclick tego buttona oraz ma logować do konsoli string "kliknięto przycisk"

// const Button = (props) => {  
//     return (
//     <button onClick={props.onButtonClick}>{props.label}</button>
//     )
// }

const Button = ({label, onButtonClick}) => {  
    return (
    <button onClick={onButtonClick}>{label}</button>
    )
}


// // 3. z podanego htmla wytworzyć aktywnie działające komponenty funkcyjne i stanowe

const HtmlTodoItem = props => {
    return (
        <li id={props.id}><h3>{props.children}</h3></li>
    )
}

/* tutaj mają być wyrenderowane elementy TodoItem dla każdego todosa */

const HtmlTodoList = props => {
    return (
        <>
        <h1>Lista:</h1>
        <ul>{props.children}</ul>
        </>
    )
}
// // ten komponent funkcyjny ma przyjmować props "title"

const ToDoItem = props => {
    return(
        <div>{props.title}</div>
    )
}

// // ten komponent stanowy ma mieć stan "imBusy" oraz "todos"
// // ten komponent ma mieć zamontowanego axiosa z getem po wszystkie todosy
// // w trakcie gdy wykonywane jest zapytanie przez axiosa ma mieć pokazany "loading"
// // jeśli zapytanie zostanie wykonane ze statusem 200 komponent ma wyświetlić listę todosów w komponentach TodoItem

class ToDoList extends React.Component {
    constructor(){
        super()
        this.state = {
            imBusy: false,
            todos: '',
        }
    }

    getToDoFromServer(){
        this.setState({
            imBusy: true
        });
        axios.get("http://195.181.210.249:3000/todo/")
        .then(response => {
            const data = response.data;
            this.setState({
                todos: data
            })
        })
        .catch(e => console.log(`Error ${e}`))
        .finally(() => {
            this.setState({
                imBusy: false
            });
        });
    }

    componentDidMount(){
        this.getToDoFromServer();
    }

    
    render(){
        const {todos, imBusy} = this.state;
        
        // element to render
        const elements = Object.keys(todos).map(key => (
            <HtmlTodoItem key={key} id={todos[key].id}>
                <ToDoItem title={todos[key].title}/>
            </HtmlTodoItem>
        ));
        
        // loader
        const loader = <div id="loaderBox" className="loader-box"><div className="lds-ripple"><div></div><div></div></div></div>

        return (
            <>
            {imBusy && loader}
            <HtmlTodoList>
                {elements}
            </HtmlTodoList>
            </>
        )
    }
}

export {
    HtmlInput,
    Input,
    HtmlButton,
    Button,
    ToDoList,
};