import React from 'react';

// "stwórz komponent stanowy ButtonWithLoaderAndSuccess"

// Część 1
// komponent ma zawierać state {imBusy,imFailed,imWithSuccess}
// komponent ma zawierać tylko jedną metodę do zmiany stanu
// komponent ma zawierać metodę onClick, włącza imBusy=true i która po 1 sek daje 50% szansy na imFailed=true i 50% szansy na imWithSuccess=true

// Część 2
// po kliknięciu w Button, opis buttona ma się zmienić na "Loading"
// po fazie loading opis buttona ma się zmienić odpowiednio na "Success" jeśli imWithSuccess=true i na "Error" jeśli imFailed=true
// komponent ma zmieniać kolory przycisku na odpowiednio szary dla "Loading", zielony dla "Success" i czerwony dla "Error" poprzez odpowiednią zmianę klasy css

class ButtonWithLoaderAndSuccess extends React.Component{
    // ...
}

// "Stwórz komponent stanowy - GoToUrl"
// jak komponent ma wyglądać - input do wpisania adresu oraz button, który wyświetli nową kartę z adresem z inputa

// Cześć 1
// komponent ma zawierać state {url, isValid}
// komponent ma zawierać 1 metodę do zmiany stanu url
// komponent ma zawierać metodę validate, która sprawdza czy wartość pod stanem url jest adresem internetowym (można wykorzystać bibliotekę is_js)

// Część 2
// po kliknięciu w button, adres ma być zwalidowany i jeśli jest poprawny to nowa karta ma zostać otwarta
// jeśli nie jest zwalidowany w inpucie na url ma się zmienić klasa na "with-error"

class GoToUrl extends React.Component{
    // ...
}


// zadanie z gwiazdką
// "stwórz komponent stanowy CheckSizeOfScreenAndComponent"

// Część 1
// komponent ma zawierać state {windowWidth, windowHeight, elementWidth, elementHeight, elementLeft, elementTop}

// Część 2
// w componentDidMount komponent powinien pobierać z elementu window oraz 'siebie' wszystkie powyższe wartości i ustawiać je w state
// w renderze tego komponentu trzeba pokazać listę ze wszystkimi nazwami stanu oraz wartościami tego stanu "name: value"
// można wykorzystać referencje do elementu html - https://reactjs.org/docs/refs-and-the-dom.html

class CheckSizeOfScreenAndComponent extends React.Component{
    // ...
}




