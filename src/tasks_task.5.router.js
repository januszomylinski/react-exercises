import React from 'react';


// Część 1
// Przy pomocy biblioteki react-router-dom zrobić aplikację App o strukturze widoków
// Wykorzystać komponenty { BrowerRouter, Link, Route, Switch, Redirect }
// struktura widoków

// /dashboard
// /dashboard/money
// /dashboard/power
// /dashboard/people

// /users
// /users/:id
// /users/:id/remove
// /users/:id/download


// Cześć 2
// Stworzyć 1 komponent ShowUrl obsługujący wszystkie widoki który ma pokazywać aktualny adres
// jeśli Router będzie na /dashboard to w ShowUrl ma się wyświetlić string '/dashboard'


// Część 3
// stworzyć w App stan state o wartości {isAuthed:false}
// napisać metodę authUser() która zmienia stan z false na true
// przekazać stan state z App do każdego z widoków


// Część 4
// stworzyć widok /login z przyciskiem który obsługuję metodę authUser()
// stworzyć logikę 2 switchy gdzie
// - jeden ma dostep do każdego widoku ale nie ma /login jeśli stan isAuthed jest true
// - jeden nie ma dostępu do żadnego widoku poza /login jeśli stan App isAuthed jest false
// - jeśli ktoś isAuthed jest false a ktoś "ręcznie" a będzie chciał wejść do routów zablokowanych, ma dostać przekierowanie na '/wtf-dude/dont-break-my-app'